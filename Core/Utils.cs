﻿using System;
using System.Linq;

namespace Core
{
    public class Utils
    {
        private static string FizzBuzz(int numberToCheck)
        {
            string returnString;
            if (numberToCheck == 0)
            {
                return numberToCheck.ToString();
            }
            bool fizz = numberToCheck % 3 == 0;
            bool buzz = numberToCheck % 5 == 0;

            if (fizz && buzz)
            {
                returnString = "FizzBuzz";
                return returnString;
            }
            else if (fizz)
            {
                returnString = "Fizz";
                return returnString;
            }
            else if (buzz)
            {
                returnString = "Buzz";
                return returnString;
            }
            else
            {
                return numberToCheck.ToString();
            }
        }

        public static string ReverseTheString(string theString)
        {
            char[] arr = theString.ToArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}